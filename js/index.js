const APP_ID = "cf26e7b2c25b5acd18ed5c3e836fb235";
const DEFAULT_VALUE = "--";

const searchInput = document.querySelector("#search-input");
const cityName = document.querySelector(".city-name");
const icon_state = document.querySelector("#icon-state");
const microphonE = document.querySelector(".microphone");
const weatherState = document.querySelector(".weather-state");
const weatherIcon = document.querySelector(".weather-icon");
const temperature = document.querySelector(".temperature");
const sunrise = document.querySelector(".sunrise");
const sunset = document.querySelector(".sunset");
const humidity = document.querySelector(".humidity");
const windSpeed = document.querySelector(".wind-speed");
const container = document.querySelector(".container");
const time = document.querySelector(".time");

searchInput.addEventListener("change", (e) => {
  fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${e.target.value}&appid=${APP_ID}&units=metric&lang=vi`
  ).then(async (res) => {
    const data = await res.json();
    console.log("[Search Input]", data);
    let currentDate = new Date();
    let minutes = currentDate.getMinutes();
    let hours = currentDate.getHours();
    let day = currentDate.getDate();
    let month = currentDate.getMonth() + 1;
    let year = currentDate.getFullYear();
    time.innerHTML = `${hours}:${minutes}-${day}/${month}/${year}`;
    cityName.innerHTML = data.name || DEFAULT_VALUE;
    weatherState.innerHTML = data.weather[0].description || DEFAULT_VALUE;
    weatherIcon.setAttribute(
      "src",
      `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`
    );
    temperature.innerHTML = Math.round(data.main.temp) || DEFAULT_VALUE;

    sunrise.innerHTML =
      moment.unix(data.sys.sunrise).format("H:mm") || DEFAULT_VALUE;
    sunset.innerHTML =
      moment.unix(data.sys.sunset).format("H:mm") || DEFAULT_VALUE;
    humidity.innerHTML = data.main.humidity || DEFAULT_VALUE;
    windSpeed.innerHTML = (data.wind.speed * 3.6).toFixed(2) || DEFAULT_VALUE;
    console.log(temperature.innerHTML);
    if (temperature.innerHTML <= 0) {
      container.style.background = "linear-gradient(145deg, #000000, #696969)";
    } else if (temperature.innerHTML >= 30) {
      container.style.background = "linear-gradient(145deg, #FFD700, #FFFFE0)";
      container.style.color = "black";
      searchInput.style.color = "black";
    } else if (data.weather[0].description.includes("mưa")) {
      container.style.background = "linear-gradient(145deg, #F5F5F5, #808080)";
    } else if (temperature.innerHTML >= 20) {
      container.style.background = "linear-gradient(145deg, #2193b0, #6dd5ed)";
    } else if (temperature.innerHTML < 20 && temperature.innerHTML > 0) {
      container.style.background = "#DCDCDC";
      container.style.color = "black";
      searchInput.style.color = "black";
    }
  });
});

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
const recognition = new SpeechRecognition();
recognition.lang = "vi-VI";
recognition.continuous = false;

const handleVoice = (text) => {
  const handleText = text.toLowerCase();
  searchInput.value = handleText;
  const changeEvent = new Event("change");
  searchInput.dispatchEvent(changeEvent);
};
const microphone = document.querySelector(".microphone");
microphone.addEventListener("click", (e) => {
  e.preventDefault();
  recognition.start();
  searchInput.setAttribute("placeholder", "Recording...");
});

recognition.onspeechend = () => {
  recognition.stop();
};

recognition.onerror = (err) => {
  console.error(err);
};

recognition.onresult = (e) => {
  console.log("onresult", e);
  const text = e.results[0][0].transcript;
  handleVoice(text);
};
